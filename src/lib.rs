/*  Copyright 2023 Novaeance
*
*   This file is part of advent_of_code_solutions.
*
*   advent_of_code_solutions is free software: you can redistribute it and/or modify it under the
*   terms of the GNU General Public License as published by the Free Software Foundation, either
*   version 3 of the License, or (at your option) any later version.
*
*   advent_of_code_solutions is distributed in the hope that it will be useful, but WITHOUT ANY
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*   PURPOSE. See the GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along with
*   advent_of_code_solutions. If not, see <https://www.gnu.org/licenses/>.
*/

#![doc = include_str!("../README.md")]

pub mod puzzles;

pub use puzzles::*;

use std::{path, fs, io::{Read, Write}};

const DEFAULT_INPUT_PATH: &str = "./data/input";
const DEFAULT_OUTPUT_PATH: &str = "./data/output";

/// Marks a struct as being able to solve an Advent of Code puzzle.
///
/// Implementing this trait allows for the [`Solver`] struct to handle a puzzle's input and output.
pub trait Solution {
    /// Creates a new instance of `Self` with arbitrary values.
    fn new() -> Self where Self: Sized;
    /// Parses a puzzle's input to allow for this struct to solve it.
    fn parse_input(&mut self, input: &str) -> Result<(), String>;
    /// Gets the solution found with the most recent use of `solve_puzzle`
    fn get_solution(&self) -> Result<String, String>;
    /// Formats the solution to a puzzle to be human readable.
    fn format_solution(&self) -> String;
    /// Finds the solution to a puzzle.
    fn solve_puzzle(&mut self);
    /// Identifies the puzzle being solved.
    fn identifier(&self) -> PuzzleIdentifier;
}

/// Identifies an Advent of Code puzzle.
///
/// # Examples
/// ```
/// # use advent_of_code_solutions::PuzzleIdentifier;
/// PuzzleIdentifier { day: 1, number: 1 }; // The first puzzle on day 1
/// PuzzleIdentifier { day: 5, number: 2 }; // The second puzzle on day 5
/// PuzzleIdentifier { day: -2, number: 8 }; // Can be outside expected ranges
/// ```
pub struct PuzzleIdentifier {
    /// The day this puzzle is part of.
    pub day: i32,
    /// Which puzzle of the day this puzzle is.
    pub number: i32,
}

enum SolverState {
    New,
    Unsolved,
    Solved,
}

/// Manages the input and output of a single Advent of Code puzzle.
///
/// The benefits of using this struct over manually handling one that implements the [`Solution`]
/// trait is that a puzzle's solution is always updated with its input, input can automatically be
/// read from files in a given path (`./data/input` by default), and output can automatically be
/// saved to a given path (`./data/output` by default).
pub struct Solver {
    puzzle: Box<dyn Solution>,
    state: SolverState,
}

impl Solver {
    /// Constructs a [`Solver`] that manages a single [`Solution`] of type `T`.
    pub fn new<T: Solution + 'static>() -> Solver {
        let puzzle = T::new();

        Solver {
            puzzle: Box::new(puzzle),
            state: SolverState::New,
        }
    }

    /// Reads the remaining contents of a file and return it as a `String`. Returns an error if one
    /// is encountered.
    pub fn read_input_file(file: &mut fs::File) -> Result<String, String> {
        let mut contents = String::new();

        if let Err(_) = file.read_to_string(&mut contents) {
            return Err("Could not read file".to_string());
        };

        Ok(contents)
    }

    /// Reads the entire contents of a file within the current input path based on the current
    /// puzzle.
    ///
    /// The files that are attempted to be read are:
    /// 1. `input_path/day{X}.txt`
    /// 2. `input_path/day{X}/puzzle{Y}.txt`
    /// 
    /// Where `{X}` is the day and `{Y}` is the number given by the [puzzle's
    /// identifier][PuzzleIdentifier].
    ///
    /// If a file is found at one of these paths, it is read and the contents of the others aren't 
    /// checked. Therefore, if the input for all puzzles on a given day are the same a file should
    /// be created at the first path, while if the input is different for any of them files should
    /// be created following the second path.
    pub fn read_default_input_file(identifier: PuzzleIdentifier) -> Result<String, String> {
        let file_path = format!("{}/day{}.txt", DEFAULT_INPUT_PATH, identifier.day);
        let folder_path = format!("{}/day{}/puzzle{}.txt", DEFAULT_INPUT_PATH, identifier.day, identifier.number);
        let file_path = path::Path::new(&file_path);
        let folder_path = path::Path::new(&folder_path);

        if let Ok(mut file) = fs::File::open(file_path) {
            return Ok(Solver::read_input_file(&mut file)?);
        }
        
        if let Ok(mut file) = fs::File::open(folder_path) {
            return Ok(Solver::read_input_file(&mut file)?);
        }

        Err(format!("Failed to load input file from default location '{:?}' or '{:?}'", file_path.as_os_str(), folder_path.as_os_str()))
    }

    /// Writes the given output to the given file. Returns an error if one is encountered.
    pub fn write_output_file(file: &mut fs::File, output: &str) -> Result<(), String> {
        match file.write_all(output.as_bytes()) {
            Ok(_) => {},
            Err(_) => {return Err("Failed to write output to file".to_string())},
        };

        Ok(())
    }

    /// Writes the given output to a file within the current output path based on the current
    /// puzzle.
    ///
    /// The file that is written to is: `output_path/day{X}/puzzle{Y}.txt` where `{X}` is the day
    /// and `{Y}` is the number given by the [puzzle's identifier][PuzzleIdentifier].
    ///
    /// If the directories required to write this file don't exist, they will be created.
    pub fn write_default_output_file(identifier: PuzzleIdentifier, output: &str) -> Result<(), String> {
        let file_directory = format!("{}/day{}", DEFAULT_OUTPUT_PATH, identifier.day);
        let file_name = format!("puzzle{}.txt", identifier.number);
        let file_path = format!("{}/{}", file_directory, file_name);

        let file_directory = path::Path::new(&file_directory);
        let file_path = path::Path::new(&file_path);

        let path_exists = match file_directory.try_exists() {
            Ok(val) => val,
            Err(_) => {return Err("Unable to verify if path exists".to_string())},
        };

        if !path_exists {
            if let Err(_) = fs::create_dir_all(file_directory) {
                return Err(format!("Failed to create directories for default output path {:?}", file_path.as_os_str()))
            }
        }

        let mut file = match fs::File::create(file_path) {
            Ok(val) => val,
            Err(_) => {return Err(format!("Failed to create or open output file at default location '{:?}'", file_path.as_os_str()))},
        };

        Solver::write_output_file(&mut file, output)?;

        Ok(())
    }
    
    /// Gives the solution to the puzzle with the current input. May instead give a reason why a
    /// solution could not be found if an error occurred when finding the solution.
    ///
    /// Requires that some form of input has been set since creating this instance. Returns an
    /// error if this has not happened.
    pub fn get_solution(&self) -> Result<String, String> {
        match self.state {
            SolverState::New => {return Err("No input has been given".to_string())},
            SolverState::Unsolved => {return Err("Unable to solve puzzle with current input".to_string())},
            SolverState::Solved => {},
        }

        match self.puzzle.get_solution() {
            Ok(val) => Ok(val.to_string()),
            Err(err) => Err(err.to_string()),
        }
    }

    /// Gives the solution to the puzzle with the current input formatted by it's [Solution] trait.
    ///
    /// The result of this method may be nonsensical if this instance's input has not been set
    /// since creation.
    pub fn get_formatted_solution(&self) -> String {
        self.puzzle.format_solution()
    }

    /// Sets the puzzle's current input and finds it's solution. Returns an error if the input
    /// couldn't be parsed or if an error occurred when finding the solution.
    pub fn set_input(&mut self, input: &str) -> Result<(), String> {
        self.puzzle.parse_input(input)?;

        self.state = SolverState::Unsolved;

        self.puzzle.solve_puzzle();
        if let Err(err) = self.puzzle.get_solution() {
            return Err(err.to_string());
        }

        self.state = SolverState::Solved;

        Ok(())
    }

    /// Attempts to read the contents of a file stored within the current input path and set it as
    /// input to the current puzzle after removing leading or trailing whitespace.
    ///
    /// Returns an error if the file couldn't be read, the input couldn't be parsed, or if an error
    /// occurred when finding a solution.
    pub fn load_default_input_file(&mut self) -> Result<(), String> {
        let identifier = self.puzzle.identifier();
        
        let contents = Solver::read_default_input_file(identifier)?;

        self.set_input(contents.trim())?;

        Ok(())
    }

    /// Attempts to read the contents of the given file and set it as input to the current puzzle
    /// after removing leading or trailing whitespace.
    pub fn load_input_from_file(&mut self, file: &mut fs::File) -> Result<(), String> {
        let contents = Solver::read_input_file(file)?;
        
        if let Err(_) = self.set_input(contents.trim()) {
            return Err("Contents of file were not valid".to_string());
        };

        Ok(())
    }

    /// Attempts to write the current puzzle's solution to a file within the current output path.
    /// Returns an error if any occur.
    pub fn write_output_to_default(&self) -> Result<(), String> {
        let identifier = self.puzzle.identifier();

        Solver::write_default_output_file(identifier, &self.get_solution()?)
    }
}
