/*  Copyright 2023 Novaeance
*
*   This file is part of advent_of_code_solutions.
*
*   advent_of_code_solutions is free software: you can redistribute it and/or modify it under the
*   terms of the GNU General Public License as published by the Free Software Foundation, either
*   version 3 of the License, or (at your option) any later version.
*
*   advent_of_code_solutions is distributed in the hope that it will be useful, but WITHOUT ANY
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*   PURPOSE. See the GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along with
*   advent_of_code_solutions. If not, see <https://www.gnu.org/licenses/>.
*/

use advent_of_code_solutions::*;
use std::io::{self, Write};

fn main() {
    let input = io::stdin();
    let mut output = io::stdout();

    println!("Please enter which day's puzzles would you like the solutions to (1-25, q to exit).");

    loop {
        print!(">> ");
        match output.flush() {
            Ok(_) => {},
            Err(_) => {},
        };

        let mut choice = String::new();
        match input.read_line(&mut choice) {
            Ok(_) => {},
            Err(_) => continue,
        }

        let choice = choice.trim().as_bytes();

        let first_byte = match choice.get(0) {
            Some(val) => val,
            None => continue,
        };

        if choice.len() == 1 {
            match first_byte {
                b'q' => break,
                b'Q' => break,
                b'1' => {
                    let mut puzzle1 = Solver::new::<day1::Puzzle1>();
                    let mut puzzle2 = Solver::new::<day1::Puzzle2>();
                    
                    load_and_print_solutions(&mut puzzle1, &mut puzzle2);
                    continue;
                },
                b'2' => {
                    let mut puzzle1 = Solver::new::<day2::Puzzle1>();
                    let mut puzzle2 = Solver::new::<day2::Puzzle2>();

                    load_and_print_solutions(&mut puzzle1, &mut puzzle2);
                    continue;
                },
                b'3' => {
                    let mut puzzle1 = Solver::new::<day3::Puzzle1>();
                    let mut puzzle2 = Solver::new::<day3::Puzzle2>();

                    load_and_print_solutions(&mut puzzle1, &mut puzzle2);
                    continue;
                },
                b'4' => {
                    let mut puzzle1 = Solver::new::<day4::Puzzle1>();
                    let mut puzzle2 = Solver::new::<day4::Puzzle2>();

                    load_and_print_solutions(&mut puzzle1, &mut puzzle2);
                    continue;
                },
                b'5' => {
                    let mut puzzle1 = Solver::new::<day5::Puzzle1>();
                    let mut puzzle2 = Solver::new::<day5::Puzzle2>();

                    load_and_print_solutions(&mut puzzle1, &mut puzzle2);
                    continue;
                },
                b'6' => {
                    let mut puzzle1 = Solver::new::<day6::Puzzle1>();
                    let mut puzzle2 = Solver::new::<day6::Puzzle2>();

                    load_and_print_solutions(&mut puzzle1, &mut puzzle2);
                    continue;
                },
                b'7' => {
                    let mut puzzle1 = Solver::new::<day7::Puzzle1>();
                    let mut puzzle2 = Solver::new::<day7::Puzzle2>();

                    load_and_print_solutions(&mut puzzle1, &mut puzzle2);
                    continue;
                },
                b'8' => {
                    println!("Day 8 puzzle 1 is not solved yet");
                    println!("Day 8 puzzle 2 is not solved yet");
                    continue;
                },
                b'9' => {
                    println!("Day 9 puzzle 1 is not solved yet");
                    println!("Day 9 puzzle 2 is not solved yet");
                    continue;
                },
                _ => {},
            }
        }

        let second_byte = match choice.get(1) {
            Some(val) => val,
            None => continue,
        };

        let pair = [first_byte, second_byte];

        if choice.len() == 2 {
            match pair {
                [b'1', b'0'] => {
                    println!("Day 10 puzzle 1 is not solved yet");
                    println!("Day 10 puzzle 2 is not solved yet");
                    continue;
                },
                [b'1', b'1'] => {
                    println!("Day 11 puzzle 1 is not solved yet");
                    println!("Day 11 puzzle 2 is not solved yet");
                    continue;
                },
                [b'1', b'2'] => {
                    println!("Day 12 puzzle 1 is not solved yet");
                    println!("Day 12 puzzle 2 is not solved yet");
                    continue;
                },
                [b'1', b'3'] => {
                    println!("Day 13 puzzle 1 is not solved yet");
                    println!("Day 13 puzzle 2 is not solved yet");
                    continue;
                },
                [b'1', b'4'] => {
                    println!("Day 14 puzzle 1 is not solved yet");
                    println!("Day 14 puzzle 2 is not solved yet");
                    continue;
                },
                [b'1', b'5'] => {
                    println!("Day 15 puzzle 1 is not solved yet");
                    println!("Day 15 puzzle 2 is not solved yet");
                    continue;
                },
                [b'1', b'6'] => {
                    println!("Day 16 puzzle 1 is not solved yet");
                    println!("Day 16 puzzle 2 is not solved yet");
                    continue;
                },
                [b'1', b'7'] => {
                    println!("Day 17 puzzle 1 is not solved yet");
                    println!("Day 17 puzzle 2 is not solved yet");
                    continue;
                },
                [b'1', b'8'] => {
                    println!("Day 18 puzzle 1 is not solved yet");
                    println!("Day 18 puzzle 2 is not solved yet");
                    continue;
                },
                [b'1', b'9'] => {
                    println!("Day 19 puzzle 1 is not solved yet");
                    println!("Day 19 puzzle 2 is not solved yet");
                    continue;
                },
                [b'2', b'0'] => {
                    println!("Day 20 puzzle 1 is not solved yet");
                    println!("Day 20 puzzle 2 is not solved yet");
                    continue;
                },
                [b'2', b'1'] => {
                    println!("Day 21 puzzle 1 is not solved yet");
                    println!("Day 21 puzzle 2 is not solved yet");
                    continue;
                },
                [b'2', b'2'] => {
                    println!("Day 22 puzzle 1 is not solved yet");
                    println!("Day 22 puzzle 2 is not solved yet");
                    continue;
                },
                [b'2', b'3'] => {
                    println!("Day 23 puzzle 1 is not solved yet");
                    println!("Day 23 puzzle 2 is not solved yet");
                    continue;
                },
                [b'2', b'4'] => {
                    println!("Day 24 puzzle 1 is not solved yet");
                    println!("Day 24 puzzle 2 is not solved yet");
                    continue;
                },
                [b'2', b'5'] => {
                    println!("Day 25 puzzle 1 is not solved yet");
                    println!("Day 25 puzzle 2 is not solved yet");
                    continue;
                },
                _ => {},
            }
        }
    }
}

fn load_and_print_solutions(puzzle1: &mut Solver, puzzle2: &mut Solver) {
    match puzzle1.load_default_input_file() {
        Ok(_) => {},
        Err(err) => {println!("Failed to load default file for puzzle 1 ({})", err)},
    };

    match puzzle2.load_default_input_file() {
        Ok(_) => {},
        Err(err) => {println!("Failed to load default file for puzzle 2 ({})", err)},
    };

    match puzzle1.get_solution() {
        Ok(val) => {println!("The solution to puzzle 1 is {}", val)},
        Err(err) => {println!("Puzzle 1 could not be solved ({})", err)},
    }

    match puzzle2.get_solution() {
        Ok(val) => {println!("The solution to puzzle 2 is {}", val)},
        Err(err) => {println!("Puzzle 2 could not be solved ({})", err)},
    }
}
