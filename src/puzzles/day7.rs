/*  Copyright 2023 Novaeance
*
*   This file is part of advent_of_code_solutions.
*
*   advent_of_code_solutions is free software: you can redistribute it and/or modify it under the
*   terms of the GNU General Public License as published by the Free Software Foundation, either
*   version 3 of the License, or (at your option) any later version.
*
*   advent_of_code_solutions is distributed in the hope that it will be useful, but WITHOUT ANY
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*   PURPOSE. See the GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along with
*   advent_of_code_solutions. If not, see <https://www.gnu.org/licenses/>.
*/

use std::cmp::Ordering;

#[derive(Debug)]
struct Card {
    value: i32,
}

impl Card {
    fn new(value: char) -> Card {
        let mut card = Card {
            value: 0,
        };

        card.set_value(value);

        card
    }

    fn set_value(&mut self, value: char) {
        match value {
            'A' => self.value = 14,
            'K' => self.value = 13,
            'Q' => self.value = 12,
            'J' => self.value = 11,
            'T' => self.value = 10,
            '9' => self.value = 9,
            '8' => self.value = 8,
            '7' => self.value = 7,
            '6' => self.value = 6,
            '5' => self.value = 5,
            '4' => self.value = 4,
            '3' => self.value = 3,
            '2' => self.value = 2,
            _ => {},
        }
    }

    fn get_value(&self) -> i32 {
        self.value
    }

    fn get_char(&self) -> char {
        match self.value {
            14 => 'A',
            13 => 'K',
            12 => 'Q',
            11 => 'J',
            10 => 'T',
            9 => '9',
            8 => '8',
            7 => '7',
            6 => '6',
            5 => '5',
            4 => '4',
            3 => '3',
            2 => '2',
            _ => ' ',
        }
    }
}
#[derive(Debug, PartialEq, PartialOrd)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

#[derive(Debug)]
struct Hand {
    cards: [Card; 5],
    bid: i32,
}

impl Hand {
    fn new(cards: [Card; 5], bid: i32) -> Hand {
        Hand {
            cards,
            bid,
        }
    }

    fn get_type(&self) -> HandType {
        let mut card_count: [i32; 13] = [0; 13];
        for card in &self.cards {
            let value = card.get_value();

            card_count[(value - 2) as usize] += 1;
        }

        let mut different_cards = 0;
        for card in card_count {
            if card != 0 {
                different_cards += 1;
            }
        }

        match different_cards {
            4 => HandType::OnePair,
            3 => {
                if card_count.contains(&2) { // Hand has 2 pairs and a single
                    HandType::TwoPair
                } else { // Hand has 2 singles and a three of a kind
                    HandType::ThreeOfAKind
                }
            },
            2 => {
                if card_count.contains(&1) { // Hand has 4 of one type and 1 of another
                    HandType::FourOfAKind
                } else { // Hand has 3 of one type and 2 of another
                    HandType::FullHouse
                }
            },
            1 => HandType::FiveOfAKind,
            _ => HandType::HighCard,
        }
    }

    fn get_value(&self) -> i32 {
        match self.get_type() {
            HandType::FiveOfAKind => 7,
            HandType::FourOfAKind => 6,
            HandType::FullHouse => 5,
            HandType::ThreeOfAKind => 4,
            HandType::TwoPair => 3,
            HandType::OnePair => 2,
            HandType::HighCard => 1,
        }
    }
}

#[derive(Debug)]
struct Game {
    hands: Vec<Hand>,
}

impl Game {
    fn new() -> Game {
        Game {
            hands: Vec::new(),
        }
    }

    fn sort_hands(&mut self) {
        self.hands.sort_unstable_by(|a, b| {
            // Sort hand by value
            match b.get_type().partial_cmp(&a.get_type()) {
                Some(val) => match val {
                    Ordering::Equal => {},
                    Ordering::Less => return Ordering::Less,
                    Ordering::Greater => return Ordering::Greater,
                },
                None => {},
            }

            // Sort hand by first card(s) value
            for i in 0..5 {
                match b.cards[i].value.cmp(&a.cards[i].value) {
                    Ordering::Equal => continue,
                    Ordering::Less => return Ordering::Less,
                    Ordering::Greater => return Ordering::Greater,
                }
            }

            Ordering::Equal
        });
    }

    fn total_winnings(&mut self) -> i32 {
        let mut winnings = 0;

        self.sort_hands();

        for (i, hand) in self.hands.iter().enumerate() {
            // Sorted from highest to lowest so multiply by maximum first
            winnings += hand.bid * (self.hands.len() - i) as i32;
        }

        winnings
    }
}

/// The first puzzle on day 7 of Advent of Code 2023.
pub struct Puzzle1 {
    game: Game,
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle1 {
    fn new() -> Self where Self: Sized {
        Self {
            game: Game::new(),
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        for line in input.split("\n") {
            let line: Vec<&str> = line.split(" ").collect();
            let cards = match line.get(0) {
                Some(val) => *val,
                None => return Err("A line is missing the hand of cards".to_string()),
            };
            let bet = match line.get(1) {
                Some(val) => *val,
                None => return Err("A line is missing the bet".to_string()),
            };

            // Parse cards
            let mut parsed_cards: [Card; 5] = [Card::new('0'), Card::new('0'), Card::new('0'), Card::new('0'), Card::new('0')];
            if cards.len() != 5 {
                return Err("A line has the incorrect number of cards".to_string());
            }
            for (i, card) in cards.chars().enumerate() {
                parsed_cards[i] = Card::new(card);
            }

            // Parse bet
            let bet = match bet.parse() {
                Ok(val) => val,
                Err(_) => return Err("A line has a bet that isn't a number".to_string()),
            };

            self.game.hands.push(Hand::new(parsed_cards, bet));
        }

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("The total winnings are {}!", val),
            Err(err) => format!("The cards got blown away by the wind... ({})", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day7::Puzzle1;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle1::new();
    /// // Example input is
    /// // AAAAA 500
    /// // J427A 5
    /// // 555AA 20
    /// // KK222 4
    /// puzzle.parse_input("AAAAA 500\nJ427A 5\n555AA 20\nKK222 3");
    /// puzzle.solve_puzzle();
    /// assert_eq!("2071", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let solution: i32;

        solution = self.game.total_winnings();

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 7, number: 1 }
    }
}

/// The second puzzle on day 7 of Advent of Code 2023.
pub struct Puzzle2 {
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle2 {
    fn new() -> Self where Self: Sized {
        Self {
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("_{}", val),
            Err(err) => format!("_{}", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day7::Puzzle2;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle2::new();
    /// puzzle.parse_input("_");
    /// puzzle.solve_puzzle();
    /// assert_eq!("0", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = 0;

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 7, number: 2 }
    }
}
