/*  Copyright 2023 Novaeance
*
*   This file is part of advent_of_code_solutions.
*
*   advent_of_code_solutions is free software: you can redistribute it and/or modify it under the
*   terms of the GNU General Public License as published by the Free Software Foundation, either
*   version 3 of the License, or (at your option) any later version.
*
*   advent_of_code_solutions is distributed in the hope that it will be useful, but WITHOUT ANY
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*   PURPOSE. See the GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along with
*   advent_of_code_solutions. If not, see <https://www.gnu.org/licenses/>.
*/

const NON_SYMBOL: char = '.';

/// The first puzzle on day 3 of Advent of Code 2023.
pub struct Puzzle1 {
    schematic: Vec<String>,
    line_length: i32,
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle1 {
    fn new() -> Self where Self: Sized {
        Self {
            schematic: Vec::new(),
            line_length: 0,
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        let mut line_length: Option<usize> = None;

        for line in input.split("\n") {
            if line_length == None {
                line_length = Some(line.len());
                // Add an extra line at the start so the middle buffer can start on the first input line
                self.schematic.push(NON_SYMBOL.to_string().repeat(line.len()));
            }

            self.schematic.push(line.to_string());

            if let Some(val) = line_length {
                if val != line.len() {
                    return Err("The lines in the schematic are different lengths".to_string());
                }
            }
        };

        // Add an extra line at the end so that the middle buffer can end on the last input line
        self.line_length = match line_length {
            Some(val) => {
                self.schematic.push(NON_SYMBOL.to_string().repeat(val));
                val as i32
            },
            None => return Err("The schematic is empty".to_string()),
        };

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("The sum of all part numbers is {}!", val),
            Err(err) => format!("The engine schematic was lost... ({})", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day3::Puzzle1;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle1::new();
    /// // Input is
    /// // ..153+8/
    /// // ..5*.#.4
    /// // 3.....12
    /// puzzle.parse_input("..153+8/\n..5*.#.4\n3.....12");
    /// puzzle.solve_puzzle();
    /// assert_eq!("182", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = 0;

        // Create line buffers
        let mut line_1: &str;
        let mut line_2: &str;
        let mut line_3: &str;

        for line_i in 0..self.schematic.len() - 2 {
            // Set line buffers to three consecutive lines
            line_1 = &self.schematic[line_i];
            line_2 = &self.schematic[line_i + 1];
            line_3 = &self.schematic[line_i + 2];
            
            let mut num = 0;
            let mut num_len = 0;
            let mut num_end_ago: Option<i32> = None;
            let mut part_num_ago: Option<i32> = None;

            // Walk along every strip of three characters in the buffered lines
            for char_i in 0..self.line_length {
                let char_i = char_i as usize;

                // Get the characters in each strip
                let char_1 = match line_1.chars().nth(char_i) {
                    Some(val) => val,
                    None => NON_SYMBOL,
                };
                
                let char_2 = match line_2.chars().nth(char_i) {
                    Some(val) => val,
                    None => NON_SYMBOL,
                };
                
                let char_3 = match line_3.chars().nth(char_i) {
                    Some(val) => val,
                    None => NON_SYMBOL,
                };

                match char_2 {
                    '0'..='9' => {
                        // Start a new number if needed
                        if num_end_ago > Some(0) {
                            num = 0;
                            num_len = 0;
                        }

                        // Append the current digit to the end of the number
                        if let Some(digit) = char_2.to_digit(10) {
                            num *= 10;
                            num += digit;
                        }

                        // Set end incase this is the last digit for this number
                        num_end_ago = Some(-1);
                        num_len += 1;
                    },
                    NON_SYMBOL => {},
                    _ => part_num_ago = Some(-1),
                }

                match char_1 {
                    '0'..='9' | NON_SYMBOL => {},
                    _ => part_num_ago = Some(-1),
                }

                match char_3 {
                    '0'..='9' | NON_SYMBOL => {},
                    _ => part_num_ago = Some(-1),
                }
                
                if let Some(val) = part_num_ago {
                    part_num_ago = Some(val + 1);
                }

                if let Some(val) = num_end_ago {
                    num_end_ago = Some(val + 1);
                }

                // If a number just ended with the previous character
                if num_end_ago == Some(1) {
                    // And the last symbol was touching it
                    if part_num_ago >= Some(0) && part_num_ago <= Some(num_len + 1) {
                        // Add the number to the solution
                        solution += num;
                    }
                }
            }

            // Add a number that was at the end of a line
            if num_end_ago == Some(0) {
                if part_num_ago >= Some(0) && part_num_ago <= Some(num_len) {
                    solution += num;
                }
            }
        }

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 3, number: 1 }
    }
}

/// The second puzzle on day 3 of Advent of Code 2023.
pub struct Puzzle2 {
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle2 {
    fn new() -> Self where Self: Sized {
        Self {
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("_{}", val),
            Err(err) => format!("_{}", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day3::Puzzle2;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle2::new();
    /// puzzle.parse_input("_");
    /// puzzle.solve_puzzle();
    /// assert_eq!("0", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = 0;

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 3, number: 2 }
    }
}
