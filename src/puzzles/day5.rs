/*  Copyright 2023 Novaeance
*
*   This file is part of advent_of_code_solutions.
*
*   advent_of_code_solutions is free software: you can redistribute it and/or modify it under the
*   terms of the GNU General Public License as published by the Free Software Foundation, either
*   version 3 of the License, or (at your option) any later version.
*
*   advent_of_code_solutions is distributed in the hope that it will be useful, but WITHOUT ANY
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*   PURPOSE. See the GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along with
*   advent_of_code_solutions. If not, see <https://www.gnu.org/licenses/>.
*/

use std::cmp;

#[derive(Debug)]
struct MapValues {
    destination_start: u32,
    source_start: u32,
    range: u32,
}

impl MapValues {
    fn new(destination_start: u32, source_start: u32, range: u32) -> MapValues {
        MapValues {
            destination_start,
            source_start,
            range,
        }
    }
}

#[derive(Debug)]
struct Map {
    ranges: Vec<MapValues>,
}

impl Map {
    fn new() -> Map {
        Map {
             ranges: Vec::new(),
        }
    }

    fn add_range(&mut self, destination_start: u32, source_start: u32, range: u32) {
        self.ranges.push(MapValues::new(destination_start, source_start, range));
    }

    fn map_value(&self, value: u32) -> u32 {
        // If value is within the range returned the mapped result, other return value
        for map in &self.ranges {
            if value >= map.source_start && value <= map.source_start + (map.range - 1) {
                let difference = value - map.source_start;
    
                return map.destination_start + difference;
            }
        }

        return value;
    }
}

#[derive(Debug)]
struct Almanac {
    seed_to_soil: Map,
    soil_to_fertilizer: Map,
    fertilizer_to_water: Map,
    water_to_light: Map,
    light_to_temperature: Map,
    temperature_to_humidity: Map,
    humidity_to_location: Map,
}

impl Almanac {
    fn new() -> Almanac {
        Almanac {
            seed_to_soil: Map::new(),
            soil_to_fertilizer: Map::new(),
            fertilizer_to_water: Map::new(),
            water_to_light: Map::new(),
            light_to_temperature: Map::new(),
            temperature_to_humidity: Map::new(),
            humidity_to_location: Map::new(),
        }
    }
}

/// The first puzzle on day 5 of Advent of Code 2023.
pub struct Puzzle1 {
    inital_seeds: Vec<u32>,
    almanac: Almanac,
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle1 {
    fn new() -> Self where Self: Sized {
        Self {
            inital_seeds: Vec::new(),
            almanac: Almanac::new(),
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        let lines: Vec<&str> = input.split("\n").collect();
        let initial_seeds = match lines.get(0) {
            Some(val) => val,
            None => return Err("Missing first line".to_string()),
        };

        // Set the initial seed values
        let initial_seeds: Vec<&str> = initial_seeds.split(" ").collect();
        for seed in initial_seeds {
            match seed {
                "seeds:" => {},
                _ => {
                    self.inital_seeds.push(match seed.parse() {
                        Ok(val) => val,
                        Err(_) => return Err("An initial seed isn't a number".to_string()),
                    })
                }
            }
        }
        
        let lines = match lines.get(1..) {
            Some(val) => val,
            None => return Err("Missing all lines except the first".to_string()),
        };

        let mut current_map: &mut Map = &mut self.almanac.seed_to_soil;

        for line in lines {
            match *line {
                // Set what map the next set of values is for
                "seed-to-soil map:" => current_map = &mut self.almanac.seed_to_soil,
                "soil-to-fertilizer map:" => current_map = &mut self.almanac.soil_to_fertilizer,
                "fertilizer-to-water map:" => current_map = &mut self.almanac.fertilizer_to_water,
                "water-to-light map:" => current_map = &mut self.almanac.water_to_light,
                "light-to-temperature map:" => current_map = &mut self.almanac.light_to_temperature,
                "temperature-to-humidity map:" => current_map = &mut self.almanac.temperature_to_humidity,
                "humidity-to-location map:" => current_map = &mut self.almanac.humidity_to_location,
                "" => {},
                _ => {
                    // Parse triplet of numbers
                    let map_values: Vec<&str> = line.split(" ").collect();

                    if map_values.len() != 3 {
                        return Err("A line has the more or less than 3 numbers".to_string());
                    }

                    let destination_start: u32 = match map_values[0].parse() {
                        Ok(val) => val,
                        Err(_) => return Err("A line has a value that isn't a number".to_string()),
                    };
                    let source_start: u32 = match map_values[1].parse() {
                        Ok(val) => val,
                        Err(_) => return Err("A line has a value that isn't a number".to_string()),
                    };
                    let range: u32 = match map_values[2].parse() {
                        Ok(val) => val,
                        Err(_) => return Err("A line has a value that isn't a number".to_string()),
                    };

                    current_map.add_range(destination_start, source_start, range);
                },
            }
        }

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("The lowest location number is {}.", val),
            Err(err) => format!("The almanac was lost... ({})", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day5::Puzzle1;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle1::new();
    /// // Example input is
    /// // seeds: 10 86
    /// //
    /// // seed-to-soil map:
    /// // 50 3 10
    /// //
    /// // soil-to-fertilizer map:
    /// // 54 19 90
    /// //
    /// // fertilizer-to-water map:
    /// // 45 9 9
    /// //
    /// // water-to-light map:
    /// // 64 41 53
    /// //
    /// // temperature-to-humidity map:
    /// // 16 16 10
    /// //
    /// // humidity-to-location map:
    /// // 90 34 52
    /// puzzle.parse_input("seeds: 10 86\n\nseed-to-soil map:\n50 3 10\n\nsoil-to-fertilizer map:\n54 19 90\n\nfertilizer-to-water map:\n45 9 9\n\nwater-to-light map:\n64 41 53\n\ntemperature-to-humidity map:\n16 16 10\n\nhumidity-to-location map:\n90 34 52");
    /// puzzle.solve_puzzle();
    /// assert_eq!("115", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = u32::MAX;

        for seed in &self.inital_seeds {
            let soil = self.almanac.seed_to_soil.map_value(*seed);
            let fertilizer = self.almanac.soil_to_fertilizer.map_value(soil);
            let water = self.almanac.fertilizer_to_water.map_value(fertilizer);
            let light = self.almanac.water_to_light.map_value(water);
            let temperature = self.almanac.light_to_temperature.map_value(light);
            let humidity = self.almanac.temperature_to_humidity.map_value(temperature);
            let location = self.almanac.humidity_to_location.map_value(humidity);

            solution = cmp::min(solution, location);
        }

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 5, number: 1 }
    }
}

/// The second puzzle on day 5 of Advent of Code 2023.
pub struct Puzzle2 {
    inital_seeds: Vec<(u32, u32)>,
    almanac: Almanac,
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle2 {
    fn new() -> Self where Self: Sized {
        Self {
            inital_seeds: Vec::new(),
            almanac: Almanac::new(),
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        let lines: Vec<&str> = input.split("\n").collect();
        let initial_seeds = match lines.get(0) {
            Some(val) => val,
            None => return Err("Missing first line".to_string()),
        };

        // Set the initial seed values
        let initial_seeds: Vec<&str> = initial_seeds.split(" ").collect();
        let initial_seeds = match initial_seeds.get(1..) {
            Some(val) => val,
            None => return Err("Missing initial seeds".to_string()),
        };
        
        for i in 0..initial_seeds.len() / 2 {
            let i = i * 2;

            let start: u32 = match initial_seeds[i].parse() {
                Ok(val) => val,
                Err(_) => return Err("Initial seed starting point isn't a number".to_string()),
            };
            let range: u32 = match initial_seeds[i + 1].parse() {
                Ok(val) => val,
                Err(_) => return Err("Inital seed range isn't a number".to_string()),
            };

            self.inital_seeds.push((start, range));
        }

        let lines = match lines.get(1..) {
            Some(val) => val,
            None => return Err("Missing all lines except the first".to_string()),
        };

        let mut current_map: &mut Map = &mut self.almanac.seed_to_soil;

        for line in lines {
            match *line {
                // Set what map the next set of values is for
                "seed-to-soil map:" => current_map = &mut self.almanac.seed_to_soil,
                "soil-to-fertilizer map:" => current_map = &mut self.almanac.soil_to_fertilizer,
                "fertilizer-to-water map:" => current_map = &mut self.almanac.fertilizer_to_water,
                "water-to-light map:" => current_map = &mut self.almanac.water_to_light,
                "light-to-temperature map:" => current_map = &mut self.almanac.light_to_temperature,
                "temperature-to-humidity map:" => current_map = &mut self.almanac.temperature_to_humidity,
                "humidity-to-location map:" => current_map = &mut self.almanac.humidity_to_location,
                "" => {},
                _ => {
                    // Parse triplet of numbers
                    let map_values: Vec<&str> = line.split(" ").collect();

                    if map_values.len() != 3 {
                        return Err("A line has the more or less than 3 numbers".to_string());
                    }

                    let destination_start: u32 = match map_values[0].parse() {
                        Ok(val) => val,
                        Err(_) => return Err("A line has a value that isn't a number".to_string()),
                    };
                    let source_start: u32 = match map_values[1].parse() {
                        Ok(val) => val,
                        Err(_) => return Err("A line has a value that isn't a number".to_string()),
                    };
                    let range: u32 = match map_values[2].parse() {
                        Ok(val) => val,
                        Err(_) => return Err("A line has a value that isn't a number".to_string()),
                    };

                    current_map.add_range(destination_start, source_start, range);
                },
            }
        }

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("The lowest location number is {}.", val),
            Err(err) => format!("The almanac was lost... ({})", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day5::Puzzle2;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle2::new();
    /// // Example input is
    /// // seeds: 10 86
    /// //
    /// // seed-to-soil map:
    /// // 50 3 10
    /// //
    /// // soil-to-fertilizer map:
    /// // 54 19 90
    /// //
    /// // fertilizer-to-water map:
    /// // 45 9 9
    /// //
    /// // water-to-light map:
    /// // 64 41 53
    /// //
    /// // temperature-to-humidity map:
    /// // 16 16 10
    /// //
    /// // humidity-to-location map:
    /// // 90 34 52
    /// puzzle.parse_input("seeds: 10 2\n\nseed-to-soil map:\n50 3 10\n\nsoil-to-fertilizer map:\n54 19 90\n\nfertilizer-to-water map:\n45 9 9\n\nwater-to-light map:\n64 41 53\n\ntemperature-to-humidity map:\n16 16 10\n\nhumidity-to-location map:\n90 34 52");
    /// puzzle.solve_puzzle();
    /// assert_eq!("115", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = u32::MAX;

        for (count, (start, range)) in self.inital_seeds.iter().enumerate() {
            println!("Pairs: {count}/{}", self.inital_seeds.len());

            for i in 0..*range {
                let seed = start + i;
                let soil = self.almanac.seed_to_soil.map_value(seed);
                let fertilizer = self.almanac.soil_to_fertilizer.map_value(soil);
                let water = self.almanac.fertilizer_to_water.map_value(fertilizer);
                let light = self.almanac.water_to_light.map_value(water);
                let temperature = self.almanac.light_to_temperature.map_value(light);
                let humidity = self.almanac.temperature_to_humidity.map_value(temperature);
                let location = self.almanac.humidity_to_location.map_value(humidity);

                solution = cmp::min(solution, location);

                if i % 1000000 == 0 {
                    println!("Range: {i}/{range}");
                    println!("Solution: {solution}");
                    println!();
                }
            }
        }

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 5, number: 2 }
    }
}
