/*  Copyright 2023 Novaeance
*
*   This file is part of advent_of_code_solutions.
*
*   advent_of_code_solutions is free software: you can redistribute it and/or modify it under the
*   terms of the GNU General Public License as published by the Free Software Foundation, either
*   version 3 of the License, or (at your option) any later version.
*
*   advent_of_code_solutions is distributed in the hope that it will be useful, but WITHOUT ANY
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*   PURPOSE. See the GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along with
*   advent_of_code_solutions. If not, see <https://www.gnu.org/licenses/>.
*/

use std::cmp;

enum CubeColours {
    Red,
    Green,
    Blue,
}

#[derive(Debug)]
struct Set {
    red_count: i32,
    green_count: i32,
    blue_count: i32,
}

impl Set {
    fn new() -> Set {
        Set {
            red_count: 0,
            green_count: 0,
            blue_count: 0,
        }
    }
    
    fn add_cubes(&mut self, to_add: (i32, CubeColours)) {
        match to_add.1 {
            CubeColours::Red => self.red_count += to_add.0,
            CubeColours::Green => self.green_count += to_add.0,
            CubeColours::Blue => self.blue_count += to_add.0,
        }
    }

    fn possible(&self, max_red: i32, max_green: i32, max_blue: i32) -> bool {
        return self.red_count <= max_red && self.green_count <= max_green && self.blue_count <= max_blue
    }

    fn min_possible(&self) -> (i32, i32, i32) {
        return (self.red_count, self.green_count, self.blue_count)
    }
}

#[derive(Debug)]
struct Game {
    sets: Vec<Set>,
}

impl Game {
    fn new() -> Game {
        Game {
            sets: Vec::new(),
        }
    }

    fn possible(&self, max_red: i32, max_green: i32, max_blue: i32) -> bool {
        for set in self.sets.iter() {
            if !set.possible(max_red, max_green, max_blue) {
                return false;
            }
        }

        return true;
    }

    fn min_possible(&self) -> (i32, i32, i32) {
        let mut game_min = (0, 0, 0);

        for set in self.sets.iter() {
            let set_min = set.min_possible();
            game_min.0 = cmp::max(game_min.0, set_min.0);
            game_min.1 = cmp::max(game_min.1, set_min.1);
            game_min.2 = cmp::max(game_min.2, set_min.2);
        }

        game_min
    }

    fn add_set(&mut self, set: Set) {
        self.sets.push(set);
    }
}

/// The first puzzle on day 2 of Advent of Code 2023.
pub struct Puzzle1 {
    games: Vec<Game>,
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle1 {
    fn new() -> Self where Self: Sized {
        Self {
            games: Vec::new(),
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        // Each line consists of a game
        for (id, line) in input.split("\n").enumerate() {
            let mut game = Game::new();
            let game_sets: Vec<&str> = line.split(": ").collect();
            let game_sets = match game_sets.get(1) {
                Some(val) => *val,
                None => return Err(format!("No cubes were drawn during game {}", id + 1)),
            };

            // Each game consists of sets of drawn blocks
            for set in game_sets.split("; ") {
                let mut set_type = Set::new();

                // Each set consists of pairs of numbers and colours
                for cubes in set.split(", ") {
                    let cubes: Vec<&str> = cubes.split(" ").collect();
                    // Parse number of cubes drawn
                    let cube_count = match cubes.get(0) {
                        Some(val) => {
                            match val.parse::<i32>() {
                                Ok(val) => val,
                                Err(_) => return Err(format!("Unable to parse number {val}")),
                            }
                        },
                        None => return Err("Missing cube count in set".to_string()),
                    };
                    // Parse colour of cubes drawn
                    let cube_colour = match cubes.get(1) {
                        Some(val) => {
                            match *val {
                                "red" => CubeColours::Red,
                                "green" => CubeColours::Green,
                                "blue" => CubeColours::Blue,
                                _ => return Err(format!("Unknown colour {val}")),
                            }
                        },
                        None => return Err("Missing cube colour in set".to_string()),
                    };

                    set_type.add_cubes((cube_count, cube_colour));
                }

                game.add_set(set_type);
            }

            self.games.push(game);
        }

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("The sum of the IDs of possible games is {}!", val),
            Err(err) => format!("The cubes were lost... ({})", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day2::Puzzle1;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle1::new();
    /// puzzle.parse_input("Game 1: 10 red, 4 green; 6 blue, 4 red, 1 green\nGame 2: 50 red\nGame 3: 5 red; 30 blue");
    /// puzzle.solve_puzzle();
    /// assert_eq!("1", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = 0;

        for (id, game) in self.games.iter().enumerate() {
            if game.possible(12, 13, 14) {
                solution += id + 1;
            }
        }

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 2, number: 1 }
    }
}

/// The second puzzle on day 2 of Advent of Code 2023.
pub struct Puzzle2 {
    games: Vec<Game>,
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle2 {
    fn new() -> Self where Self: Sized {
        Self {
            games: Vec::new(),
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        // Each line consists of a game
        for (id, line) in input.split("\n").enumerate() {
            let mut game = Game::new();
            let game_sets: Vec<&str> = line.split(": ").collect();
            let game_sets = match game_sets.get(1) {
                Some(val) => *val,
                None => return Err(format!("No cubes were drawn during game {}", id + 1)),
            };

            // Each game consists of sets of drawn blocks
            for set in game_sets.split("; ") {
                let mut set_type = Set::new();

                // Each set consists of pairs of numbers and colours
                for cubes in set.split(", ") {
                    let cubes: Vec<&str> = cubes.split(" ").collect();
                    // Parse number of cubes drawn
                    let cube_count = match cubes.get(0) {
                        Some(val) => {
                            match val.parse::<i32>() {
                                Ok(val) => val,
                                Err(_) => return Err(format!("Unable to parse number {val}")),
                            }
                        },
                        None => return Err("Missing cube count in set".to_string()),
                    };
                    // Parse colour of cubes drawn
                    let cube_colour = match cubes.get(1) {
                        Some(val) => {
                            match *val {
                                "red" => CubeColours::Red,
                                "green" => CubeColours::Green,
                                "blue" => CubeColours::Blue,
                                _ => return Err(format!("Unknown colour {val}")),
                            }
                        },
                        None => return Err("Missing cube colour in set".to_string()),
                    };

                    set_type.add_cubes((cube_count, cube_colour));
                }

                game.add_set(set_type);
            }

            self.games.push(game);
        }

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("The sum of the produce of each minimum set of cubes is {}!", val),
            Err(err) => format!("The cubes were lost... ({})", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day2::Puzzle2;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle2::new();
    /// puzzle.parse_input("Game 1: 10 red, 4 green; 6 blue, 4 red, 1 green\nGame 2: 50 red\nGame 3: 5 red; 30 blue");
    /// puzzle.solve_puzzle();
    /// assert_eq!("240", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = 0;

        for game in self.games.iter() {
            let min_possible = game.min_possible();

            solution += min_possible.0 * min_possible.1 * min_possible.2;
        }

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 2, number: 2 }
    }
}
