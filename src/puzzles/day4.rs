/*  Copyright 2023 Novaeance
*
*   This file is part of advent_of_code_solutions.
*
*   advent_of_code_solutions is free software: you can redistribute it and/or modify it under the
*   terms of the GNU General Public License as published by the Free Software Foundation, either
*   version 3 of the License, or (at your option) any later version.
*
*   advent_of_code_solutions is distributed in the hope that it will be useful, but WITHOUT ANY
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*   PURPOSE. See the GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along with
*   advent_of_code_solutions. If not, see <https://www.gnu.org/licenses/>.
*/

use std::collections::VecDeque;

#[derive(Debug)]
struct Card {
    card_number: i32,
    winning_numbers: Vec<i32>,
    your_numbers: Vec<i32>,
}

impl Card {
    fn new(card_number: i32) -> Card {
        Card {
            card_number,
            winning_numbers: Vec::new(),
            your_numbers: Vec::new(),
        }
    }

    fn push_winning_number(&mut self, number: i32) {
        self.winning_numbers.push(number)
    }

    fn push_your_number(&mut self, number: i32) {
        self.your_numbers.push(number)
    }

    fn get_value(&self) -> i32 {
        let mut value = 0;

        for yours in &self.your_numbers {
            for winning in &self.winning_numbers {
                if yours == winning {
                    match value {
                        0 => value = 1,
                        _ => value *= 2,
                    }
                }
            }
        }

        value
    }
}

/// The first puzzle on day 4 of Advent of Code 2023.
pub struct Puzzle1 {
    solution: Result<String, String>,
    cards: Vec<Card>,
}

impl crate::Solution for Puzzle1 {
    fn new() -> Self where Self: Sized {
        Self {
            solution: Ok("".to_string()),
            cards: Vec::new(),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        for line in input.split("\n") {
            // Split the label from the numbers
            let label_and_numbers: Vec<&str> = line.split(": ").collect();
            let label = match label_and_numbers.get(0) {
                Some(val) => val,
                None => return Err("A line is missing its label".to_string()),
            };
            let numbers = match label_and_numbers.get(1) {
                Some(val) => val,
                None => return Err("A line is missing its numbers".to_string()),
            };

            // Parse the label
            let label: Vec<&str> = label.split(" ").collect();
            let card_num = match label.get(label.len() - 1) {
                Some(val) => val,
                None => return Err("A line has a card without an identifier".to_string()),
            };

            let card_num: i32 = match card_num.parse() {
                Ok(val) => val,
                Err(_) => return Err("A line has a card without a numerical identifier".to_string()),
            };

            let mut card = Card::new(card_num);

            // Parse the numbers
            let numbers: Vec<&str> = numbers.split(" | ").collect();
            let your_numbers = match numbers.get(0) {
                Some(val) => val,
                None => return Err("A line is missing your numbers".to_string()),
            };
            let winning_numbers = match numbers.get(1) {
                Some(val) => val,
                None => return Err("A line is missing the winning numbers".to_string()),
            };

            for num in your_numbers.split(" ") {
                let num: i32 = match num.parse() {
                    Ok(val) => val,
                    Err(_) => continue,
                };

                card.push_your_number(num);
            }

            for num in winning_numbers.split(" ") {
                let num: i32 = match num.parse() {
                    Ok(val) => val,
                    Err(_) => continue,
                };

                card.push_winning_number(num);
            }

            self.cards.push(card);
        }

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("The cards are worth a total of {} points!", val),
            Err(err) => format!("The cards were lost {}...", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day4::Puzzle1;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle1::new();
    /// // Input is
    /// // Card 1: 50 40 88 65  5  7 |  5 40  7 50  6 87
    /// // Card 2: 10  5 |  1
    /// puzzle.parse_input("Card 1: 50 40 88 65  5  7 |  5 40  7 50 6 87\nCard 2: 10  5 |  1");
    /// puzzle.solve_puzzle();
    /// assert_eq!("8", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = 0;

        for card in &self.cards {
            solution += card.get_value();
        }

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 4, number: 1 }
    }
}

#[derive(Debug)]
struct Queue {
    data: VecDeque<i32>,
}

impl Queue {
    fn new() -> Queue {
        Queue {
            data: VecDeque::new()
        }
    }

    fn increment(&mut self, index: usize) {
        // Only allow for new entries to be added directly after the current last entry
        if index > self.data.len() + 1 {
            panic!("Index is out of bounds");
        }

        // Increment value at index
        let value = self.data.get_mut(index);
        let value = match value {
            Some(val) => val,
            None => {
                self.data.push_back(1);
                return;
            },
        };

        *value += 1;
    } 

    fn pop(&mut self) -> Option<i32> {
        self.data.pop_front()
    }
}

/// The second puzzle on day 4 of Advent of Code 2023.
pub struct Puzzle2 {
    solution: Result<String, String>,
    cards: Vec<Card>,
}

impl crate::Solution for Puzzle2 {
    fn new() -> Self where Self: Sized {
        Self {
            solution: Ok("".to_string()),
            cards: Vec::new(),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        for line in input.split("\n") {
            // Split the label from the numbers
            let label_and_numbers: Vec<&str> = line.split(": ").collect();
            let label = match label_and_numbers.get(0) {
                Some(val) => val,
                None => return Err("A line is missing its label".to_string()),
            };
            let numbers = match label_and_numbers.get(1) {
                Some(val) => val,
                None => return Err("A line is missing its numbers".to_string()),
            };

            // Parse the label
            let label: Vec<&str> = label.split(" ").collect();
            let card_num = match label.get(label.len() - 1) {
                Some(val) => val,
                None => return Err("A line has a card without an identifier".to_string()),
            };

            let card_num: i32 = match card_num.parse() {
                Ok(val) => val,
                Err(_) => return Err("A line has a card without a numerical identifier".to_string()),
            };

            let mut card = Card::new(card_num);

            // Parse the numbers
            let numbers: Vec<&str> = numbers.split(" | ").collect();
            let your_numbers = match numbers.get(0) {
                Some(val) => val,
                None => return Err("A line is missing your numbers".to_string()),
            };
            let winning_numbers = match numbers.get(1) {
                Some(val) => val,
                None => return Err("A line is missing the winning numbers".to_string()),
            };

            for num in your_numbers.split(" ") {
                let num: i32 = match num.parse() {
                    Ok(val) => val,
                    Err(_) => continue,
                };

                card.push_your_number(num);
            }

            for num in winning_numbers.split(" ") {
                let num: i32 = match num.parse() {
                    Ok(val) => val,
                    Err(_) => continue,
                };

                card.push_winning_number(num);
            }

            self.cards.push(card);
        }

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("You end up with a total of {} cards!", val),
            Err(err) => format!("The cards were lost {}...", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day4::Puzzle2;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle2::new();
    /// // Input is
    /// // Card 1: 50 40 88 65  5  7 | 45 40 37 90  6 87
    /// // Card 2: 10  5 |  1
    /// puzzle.parse_input("Card 1: 50 40 88 65  5  7 | 45 40 37 90 6 87\nCard 2: 10  5 |  1");
    /// puzzle.solve_puzzle();
    /// assert_eq!("3", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = 0;
        let mut card_count = Queue::new(); 

        for card in &self.cards {
            // Always have at least 1 of the current card
            card_count.increment(0);

            let mut value = card.get_value();
            let mut num_of_winning = 0;

            // Find the number of winning numbers
            while value > 1 {
                value /= 2;
                num_of_winning += 1;
            }

            // Add 1 or 0 depending on the left over value
            num_of_winning += value;

            // Get the number of copies of this card
            let num_of_cards = match card_count.pop() {
                Some(val) => val,
                None => 0,
            };

            // For each copy and the original
            for _ in 0..num_of_cards {
                // Increment the number of upcoming copies
                for i in 0..num_of_winning {
                    card_count.increment(i as usize);
                }

                // Add to the number of cards used so far
                solution += 1;
            }
        }

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 4, number: 2 }
    }
}
