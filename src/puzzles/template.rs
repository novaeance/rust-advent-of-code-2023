/*  Copyright 2023 Novaeance
*
*   This file is part of advent_of_code_solutions.
*
*   advent_of_code_solutions is free software: you can redistribute it and/or modify it under the
*   terms of the GNU General Public License as published by the Free Software Foundation, either
*   version 3 of the License, or (at your option) any later version.
*
*   advent_of_code_solutions is distributed in the hope that it will be useful, but WITHOUT ANY
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*   PURPOSE. See the GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along with
*   advent_of_code_solutions. If not, see <https://www.gnu.org/licenses/>.
*/

/// The first puzzle on day _ of Advent of Code 2023.
pub struct Puzzle1 {
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle1 {
    fn new() -> Self where Self: Sized {
        Self {
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("_{}", val),
            Err(err) => format!("_{}", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day_::Puzzle1;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle1::new();
    /// puzzle.parse_input("_");
    /// puzzle.solve_puzzle();
    /// assert_eq!("0", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = 0;

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: _, number: 1 }
    }
}

/// The second puzzle on day _ of Advent of Code 2023.
pub struct Puzzle2 {
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle2 {
    fn new() -> Self where Self: Sized {
        Self {
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("_{}", val),
            Err(err) => format!("_{}", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day_::Puzzle2;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle2::new();
    /// puzzle.parse_input("_");
    /// puzzle.solve_puzzle();
    /// assert_eq!("0", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = 0;

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: _, number: 2 }
    }
}
