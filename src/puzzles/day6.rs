/*  Copyright 2023 Novaeance
*
*   This file is part of advent_of_code_solutions.
*
*   advent_of_code_solutions is free software: you can redistribute it and/or modify it under the
*   terms of the GNU General Public License as published by the Free Software Foundation, either
*   version 3 of the License, or (at your option) any later version.
*
*   advent_of_code_solutions is distributed in the hope that it will be useful, but WITHOUT ANY
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*   PURPOSE. See the GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along with
*   advent_of_code_solutions. If not, see <https://www.gnu.org/licenses/>.
*/

#[derive(Debug)]
struct Race {
    time: u64,
    record: u64,
}

impl Race {
    fn new(time: u64, record: u64) -> Race {
        Race {
            time,
            record,
        }
    }

    fn beat_record(&self) -> Vec<u64> {
        // Get how long the button could have been held to get the record
        let record_held = match self.calculate_held(self.record) {
            Ok(val) => val,
            Err(_) => return vec![0],
        };

        // Generate list of times to hold the button for the beat the record
        let mut result = Vec::with_capacity((record_held.1 - record_held.0) as usize);
        for i in record_held.0 + 1..record_held.1 {
            result.push(i)
        }

        result
    }

    fn calculate_distance(&self, held_time: u64) -> u64 {
        // distance = speed * time left
        // time left = time - held time
        // speed = held time mm/s
        // therefore, distance = held time * (time - held time)
        held_time * (self.time - held_time)
    }

    fn calculate_held(&self, distance: u64) -> Result<(u64, u64), String> {
        // distance = held time * (time - held time) (see calculate_distance)
        // 0 = -held time^2 + time * held time - distance
        // therefore, h = (-time +- sqrt(time^2 - 4 * distance)) / -2 (quadratic formula)
        let square_root = (self.time.pow(2) - 4 * distance) as f64;
        if square_root < 0.0 {
            return Err("Distance can't be greater than furthest possible distance".to_string());
        }
        let square_root = square_root.sqrt();

        let held = (
            // Round down so value is always under the record
            ((-(self.time as f64) + square_root) / -2.0) as u64,
            // Round up so value is always under the record
            ((-(self.time as f64) - square_root) / -2.0).ceil() as u64
        );

        return Ok(held);
    }
}

/// The first puzzle on day 6 of Advent of Code 2023.
pub struct Puzzle1 {
    races: Vec<Race>,
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle1 {
    fn new() -> Self where Self: Sized {
        Self {
            races: Vec::new(),
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        // Get the lines with the times and records on them
        let lines: Vec<&str> = input.split("\n").collect();
        let times = match lines.get(0) {
            Some(val) => *val,
            None => return Err("There is nothing in the input".to_string()),
        };
        let records = match lines.get(1) {
            Some(val) => *val,
            None => return Err("There is only one line in the input".to_string()),
        };

        // Parsed the numbers from the times line
        let mut nums: Vec<u64> = Vec::new();
        for ch in times.split(" ") {
            nums.push(match ch.parse() {
                Ok(val) => val,
                Err(_) => continue,
            });
        }
        let times = nums.clone();

        nums.clear();

        // Parse the numbers from the records line
        for ch in records.split(" ") {
            nums.push(match ch.parse() {
                Ok(val) => val,
                Err(_) => continue,
            });
        }
        let records = nums;

        // Check the number of times and records is the same
        if times.len() != records.len() {
            return Err("There is a different number of times and distances in the input".to_string());
        }

        // Add races to the puzzle
        for i in 0..times.len() {
            self.races.push(Race::new(times[i], records[i]));
        }

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("The multiple of all ways to beat the records is {}!", val),
            Err(err) => format!("The buttons are broken... ({})", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day6::Puzzle1;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle1::new();
    /// // Input is:
    /// // Time:      3   34   68
    /// // Distance:  1  116  890
    /// puzzle.parse_input("Time:      3   34   68\nDistance:  1  116  890");
    /// puzzle.solve_puzzle();
    /// assert_eq!("1782", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = 1;

        for race in &self.races {
            let beat_record = race.beat_record();
            solution *= beat_record.len();
        }

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 6, number: 1 }
    }
}

/// The second puzzle on day 6 of Advent of Code 2023.
pub struct Puzzle2 {
    race: Race,
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle2 {
    fn new() -> Self where Self: Sized {
        Self {
            race: Race::new(0, 0),
            solution: Ok("".to_string()),
        }
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        // Get the lines with the times and records on them
        let lines: Vec<&str> = input.split("\n").collect();
        let time = match lines.get(0) {
            Some(val) => *val,
            None => return Err("There is nothing in the input".to_string()),
        };
        let record = match lines.get(1) {
            Some(val) => *val,
            None => return Err("There is only one line in the input".to_string()),
        };

        // Parsed the number from the times line
        let mut num: u64 = 0;
        for ch in time.split(" ") {
             let digits: u64 = match ch.parse() {
                Ok(val) => val,
                Err(_) => continue,
            };

            // Shift the current num to the left ch.len() times
            for _ in 0..ch.len() {
                num *= 10;
            }
            num += digits;
        }
        let time = num;

        num = 0;

        // Parse the numbers from the records line
        for ch in record.split(" ") {
             let digits: u64 = match ch.parse() {
                Ok(val) => val,
                Err(_) => continue,
            };

            // Shift the current num to the left ch.len() times
            for _ in 0..ch.len() {
                num *= 10;
            }
            num += digits;
        }
        let record = num;

        // Add race to the puzzle
        self.race = Race::new(time, record);

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("The record can be beaten in {} different ways!", val),
            Err(err) => format!("The buttons are broken... ({})", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day6::Puzzle2;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle2::new();
    /// // Input is:
    /// // Time:      3   34   68
    /// // Distance:  1  116  890
    /// puzzle.parse_input("Time:      3   34   68\nDistance:  1  116  890");
    /// puzzle.solve_puzzle();
    /// assert_eq!("33401", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let solution: u32;

        let beat_record = self.race.beat_record();
        solution = beat_record.len() as u32;

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 6, number: 1 }
    }
}
