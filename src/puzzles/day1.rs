/*  Copyright 2023 Novaeance
*
*   This file is part of advent_of_code_solutions.
*
*   advent_of_code_solutions is free software: you can redistribute it and/or modify it under the
*   terms of the GNU General Public License as published by the Free Software Foundation, either
*   version 3 of the License, or (at your option) any later version.
*
*   advent_of_code_solutions is distributed in the hope that it will be useful, but WITHOUT ANY
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*   PURPOSE. See the GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along with
*   advent_of_code_solutions. If not, see <https://www.gnu.org/licenses/>.
*/

/// The first puzzle on day 1 of Advent of Code 2023.
pub struct Puzzle1 {
    values: String,
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle1 {
    fn new() -> Self where Self: Sized {
        let new = Self {
            values: "".to_string(),
            solution: Ok("".to_string()),
        };

        new
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        self.values = input.to_string();

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("The sum of all calibration values is {}!", val),
            Err(err) => format!("The calibration values could not be recovered... ({})", err.to_string()),
        }
    }

    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day1::Puzzle1;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle1::new();
    /// puzzle.parse_input("123456789\na6cd3fghi\no1e");
    /// puzzle.solve_puzzle();
    /// assert_eq!("93", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    /// ```
    fn solve_puzzle(&mut self) {
        let mut solution = 0;

        for line in self.values.split("\n") {
            let mut first_num = 0;
            let mut last_num = 0;
            let mut found_first = false;

            // Search digits
            for digit in line.chars() {
                match digit {
                    '0'..='9' => {
                        // Parse digit to a u32
                        let digit = match digit.to_digit(10) {
                            Some(val) => val,
                            None => {
                                self.solution = Err(format!("Failed to parse digit {digit}"));
                                return;
                            }
                        };

                        if !found_first {
                            first_num = digit;
                            found_first = true;
                        }

                        last_num = digit;
                    },
                    _ => {},
                }
            }

            if !found_first {
                self.solution = Err(format!("Unable to find any digit in line {line}"));
                return;
            }

            // Treat first_num as first digit in a two digit number
            solution += first_num * 10 + last_num;
        }

        self.solution = Ok(solution.to_string());
    }

    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 1, number: 1 }
    }
}

/// The second puzzle on day 1 of Advent of Code 2023.
pub struct Puzzle2 {
    values: String,
    solution: Result<String, String>,
}

impl crate::Solution for Puzzle2 {
    fn new() -> Self where Self: Sized {
        let new = Self {
            values: "".to_string(),
            solution: Ok("".to_string()),
        };

        new
    }

    fn parse_input(&mut self, input: &str) -> Result<(), String> {
        self.values = input.to_string();

        Ok(())
    }

    fn get_solution(&self) -> Result<String, String> {
        self.solution.clone()
    }

    fn format_solution(&self) -> String {
        match self.solution.clone() {
            Ok(val) => format!("The sum of all calibration values is {}.", val),
            Err(err) => format!("The calibration values could not be recovered... ({})", err.to_string()),
        }
    }
    
    /// Finds the solution to a puzzle.
    ///
    /// # Examples
    ///
    /// ```
    /// # use advent_of_code_solutions::day1::Puzzle2;
    /// # use advent_of_code_solutions::Solution;
    /// # pub fn main() -> Result<(), String> {
    /// let mut puzzle = Puzzle2::new();
    /// puzzle.parse_input("onetwothreefour\none2three4\na6cdef");
    /// puzzle.solve_puzzle();
    /// assert_eq!("94", puzzle.get_solution()?);
    /// # Ok(())
    /// # }
    ///
    fn solve_puzzle(&mut self) {
        let mut solution = 0;

        for line in self.values.split("\n") {
            let mut first_num = 0;
            let mut last_num = 0;
            let mut found_first = false;
            let mut padded_line = line.to_string();
            let mut padding = String::new();

            // Add padding to allow a slice of 5 characters to start at the end of a line
            for _ in 0..4 {
                padding.push('_');
            }
            padded_line.push_str(&padding);

            // Search padded_line using slices one, three, four, and five characters long
            for i in 0..=padded_line.len() - 5 {
                let slice = &padded_line[i..i + 5];
                let one_character = &slice[0..1];
                let three_characters = &slice[0..3];
                let four_characters = &slice[0..4];
                let five_characters = &slice[0..5];

                // Try to parse the value as a literal digit (0-9)
                let mut digit = match one_character.parse::<i32>() {
                    Ok(val) => Some(val),
                    Err(_) => None,
                };

                // If needed, try to parse the value as a three lettered number
                if digit == None {
                    digit = match three_characters {
                        "one" => Some(1),
                        "two" => Some(2),
                        "six" => Some(6),
                        _ => None,
                    };
                }

                // If needed, try to parse the value as a four lettered number
                if digit == None {
                    digit = match four_characters {
                        "four" => Some(4),
                        "five" => Some(5),
                        "nine" => Some(9),
                        _ => None,
                    };
                }

                // if needed, try to parse the value as a five lettered number
                if digit == None {
                    digit = match five_characters {
                        "three" => Some(3),
                        "seven" => Some(7),
                        "eight" => Some(8),
                        _ => None,
                    };
                }

                // Continue search for a digit if one wasn't found
                let digit = match digit {
                    Some(val) => val,
                    None => continue,
                };

                if !found_first {
                    first_num = digit;
                    found_first = true;
                }

                last_num = digit;
            }

            // Treat first_num as first digit in a two digit number
            solution += first_num * 10 + last_num;
        }

        self.solution = Ok(solution.to_string());
    }
    
    fn identifier(&self) -> crate::PuzzleIdentifier {
        crate::PuzzleIdentifier { day: 1, number: 2 }
    }
}
