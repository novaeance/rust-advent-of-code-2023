# Rust Advent of Code 2023
This crate contains a library that aims to make writing code to solve [Advent of Code](https://adventofcode.com/2023/about) puzzles easier. A binary is also included that allows for the results of the included solutions to be viewed. Throughout December 2023, solutions to each day's puzzles will be added. Currently solved 12 out of 50 puzzles.

## Please Note
This project is not being maintained with the intent that others will use and rely upon it. While an effort is made to keep documentation up to date and to minimise API changes, no guarantees that this will happen are made. Therefore, usage of this crate is only encouraged from a standpoint of code inspection and experimentation.

# Installation and Usage Instructions
## Please Note
These instructions are mainly written as an exercise for the author. Following them is only encouraged from the standpoint of code inspection and experimentation.

## Solution Viewer Binary
To run this crate's binary, first clone this repo with the command `git clone https://gitlab.com/novaeance/rust-advent-of-code-2023.git`

Then navigate into the newly created folder with `cd rust-advent-of-code-2023`

Then create a new folder with the command `mkdir data/input/` and place your puzzle input(s) for days where the input of each puzzle is:
- The same, within a file at `data/input/day1.txt`
- Different, within a file at `data/input/day1/puzzle1.txt` and `data/input/day1/puzzle2.txt`

Be sure to replace the 1 in `day1` with the relevant day's number and that your puzzle input is formatted the same as the given puzzle's input is on the Advent of Code website.

Finally, use the command `cargo run` to build and run the crate's binary.

## Solver Library
To use this crate's library, first navigate into a rust project and open it's `Cargo.toml` file. (A new project can be created with the command `cargo new crate_name`).

Then add the line `advent_of_code_solutions = { git = "https://gitlab.com/novaeance/rust-advent-of-code-2023.git" }` underneath the `[dependencies]` tag.

Finally, build the crate with `cargo build` to add this crate to your project.

Documentation for this crate can be viewed by running the command `cargo doc --open` and then selecting this crate from the side menu.

# Credit
[Advent of Code](https://adventofcode.com/2023/about) is created by [Eric Wastl](http://was.tl/).

# License
This crate is licensed under [GPL V3](https://www.gnu.org/licenses/gpl-3.0.txt) or later.
